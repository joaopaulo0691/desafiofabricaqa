package TesteAutomatizado;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BasicForm {

    private WebDriver driver;

    // Inicialmente foi feito testes individuais para cada funcionalidade
    // Por fim, o último método testFillTheForm(), preenche o formulário por completo e dá submit.

    @Before
    public void abrirHtmlForm(){
        System.setProperty("webdriver.gecko.driver", "C:/Users/João Paulo/Documents/desafiofabricaqa/src/Driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize(); // o teste abrirá em tela cheia na página selecionada
        driver.get("http://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }

    @After
    public void fecharHtmlForm(){driver.quit();}

    @Test
    public void testInsertText() throws InterruptedException {
        driver.findElement(By.name("username")).sendKeys("João Paulo");
        driver.findElement(By.name("password")).sendKeys("1234");
        driver.findElement(By.name("comments")).sendKeys("Desafio Fábrica de Software");
        Thread.sleep(5000);
    }

    @Test
    public void testClickCheckBox() throws InterruptedException {
        //desselecionando a checkBox 3, primeiro. Quando o form abrir, ele já vem com alternativa
        // pré - selecionada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        //Selecionando só a opção 2
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        Thread.sleep(5000);
    }

    @Test
    public void testRadioButton() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();
        Thread.sleep(5000);
    }

    @Test
    public void testMultipleSelectOption() throws InterruptedException {
        // Aqui acontece a msm coisa do checkbox, com a opção 4 selecionada, sendo ela desmarcada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        //Após isso só a opção 2 é selecionada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(5000);
    }

    @Test
    public void testSubmit() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(5000);
    }

    @Test
    public void testFillTheForm() throws InterruptedException {
        // preenchimento completo do formulário
        driver.findElement(By.name("username")).sendKeys("João Paulo");
        driver.findElement(By.name("password")).sendKeys("1234");
        driver.findElement(By.name("comments")).sendKeys("Desafio Fábrica de Software");
        //desselecionando a checkBox 3, primeiro. Quando o form abrir, ele já vem com alternativa
        // pré - selecionada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        //Selecionando só a opção 2
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        Thread.sleep(5000);
        //Radio Button
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();
        // opção 4 selecionada, sendo ela desmarcada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        //Após isso só a opção 2 é selecionada
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(5000);

    }





}
